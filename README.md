# common-tools



## 介绍
力求用最小的依赖, 来实现短小精悍的瑞士军刀



## 依赖
```xml
<dependency>
    <groupId>io.gitee.minelx</groupId>
    <artifactId>common-tools</artifactId>
    <version>1.2.1</version>
</dependency>
```



## 使用说明

### 1. date 日期组件



#### 第一个例子: 查询 2000年 到 3000年 中间有多少个闰年

```java
io.gitee.minelx.commontools.date.example.FindLeapYearsBetweenARange
```



> 全文

```java
long yyyyMM = YEAR.at(2000).to(YEAR.at(3000))
        .clocks(YEAR, 1)
        .map(each -> each.elapse(_locate(MONTH, 3).andThen(_offset(-1, DAY))))
        .filter(which -> which.values().day() > 28)
        .map(each -> each.format("yyyy"))
        .count();
System.out.println("yyyyMM = " + yyyyMM);
```



> ClockUnit.at(int... values) 返回了一个精确到某个 时间单位(ClockUnit) 的 时间点(Clock)
```java
YEAR.at(2000) // 我得到了一个 2000年 的时间点
```



> Clock.to(Clock destination) 需要提供时间终点以构建一个 时间段(Duration)
```java
YEAR.at(2000).to(YEAR.at(3000)) // 我获得了2000年到3000年的时间段
```



> Duration.clocks(ClockUnit unit, int amount) 在这个时间段里推算出给定 时间间隔(offset) 的所有时间点
```java
YEAR.at(2000).to(YEAR.at(3000))
        .clocks(YEAR, 1) // 我获得了2000年到3000年之间的每一年 (前闭后开)
```



> Clock.elapse(Consumer<Calendar\> elapsing) 使用提供的 偏移器 来制造并返回偏移后的时间点
```java
each.elapse(_locate(MONTH, 3).andThen(_offset(-1, DAY))) // 我获得了 2月底(最后一天) 的时间点
```



> Clock.values() 获得该时间点的数值映射
```java
which.values().day() > 28 // 该时间点以天数判断是否是闰年
```



> Clock.format(String formatText) 使用字符串格式化该时间点
```java
each.format("yyyy") // yyyy
```



#### 第二个例子: 获得现在的时间

```java
io.gitee.minelx.commontools.date.example.WhatsInClocks
```



> 全文

```java
String now = Clocks.now().format("yyyy/MM/dd hh:mm:ss");
System.out.println("now = " + now);

String millis = Clocks.millis(1640966400000L).format("yyyy/MM/dd hh:mm:ss");
System.out.println("millis = " + millis);

String unixTime = Clocks.unixTime(1640966400).format("yyyy/MM/dd hh:mm:ss");
System.out.println("unixTime = " + unixTime);
```


> Clocks.now() 返回当前时间点

```java
Clocks.now()
```


> Clocks.millis(long timeMillis) 使用java时间戳(毫秒级)来构建时间点

```java
Clocks.millis(1640966400000L)
```


> Clocks.unixTime(int timestamp) 使用unix timestamp来构建时间点

```java
Clocks.unixTime(1640966400)
```

