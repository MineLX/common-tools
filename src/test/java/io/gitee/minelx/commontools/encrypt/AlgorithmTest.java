package io.gitee.minelx.commontools.encrypt;

import io.gitee.minelx.commontools.encrypt.cipher.algorithm.Algorithm;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class AlgorithmTest {
	@Test
	void from() {
		assertEquals("DES", Algorithm.from("DES").name());
		assertEquals("AES", Algorithm.from("AES").name());
	}
}