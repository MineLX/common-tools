package io.gitee.minelx.commontools.encrypt;

import io.gitee.minelx.commontools.encrypt.cipher.algorithm.Algorithm;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class DESAlgorithmTest {
	@Test
	void algorithm() {
		assertEquals("DES", Algorithm.des().name());
	}
}
