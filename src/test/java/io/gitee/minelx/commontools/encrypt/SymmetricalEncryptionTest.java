package io.gitee.minelx.commontools.encrypt;

import io.gitee.minelx.commontools.encrypt.cipher.CipherPattern;
import org.junit.jupiter.api.Test;

import static io.gitee.minelx.commontools.encrypt.cipher.CipherConstant.Algorithm.AES;
import static io.gitee.minelx.commontools.encrypt.cipher.CipherConstant.Algorithm.DES;
import static io.gitee.minelx.commontools.encrypt.cipher.CipherConstant.Padding.NOPE;
import static io.gitee.minelx.commontools.encrypt.cipher.CipherConstant.Padding.PKCS_5;
import static io.gitee.minelx.commontools.encrypt.cipher.CipherConstant.Strategy.CBC;
import static io.gitee.minelx.commontools.encrypt.cipher.CipherConstant.Strategy.ECB;
import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.fail;

class SymmetricalEncryptionTest {

	@Test
	void aes_cbc() {
		/* password should be multiple of 16 if we are using AES */
		verifySymmetrically(SymmetricalEncryption.cipher(CipherPattern.of(AES, CBC, PKCS_5).passwordAndIV(CryptoTestConstants.MULTIPLE_OF_16_PASSWORD, CryptoTestConstants.MULTIPLE_OF_16_PASSWORD)), "1");
	}

	@Test
	void des_cbc_with_no_padding() {
		SymmetricalEncryption desCBCWithNoPadding = SymmetricalEncryption.cipher(
				CipherPattern.of(DES, CBC, NOPE).passwordAndIV(
						CryptoTestConstants.MULTIPLE_OF_8_PASSWORD,
						CryptoTestConstants.MULTIPLE_OF_8_PASSWORD));
		verifySymmetrically(desCBCWithNoPadding, CryptoTestConstants.MULTIPLE_OF_8_RAW_DATA /* raw input should be multiple of 8 if we don't use padding. */);
		try {
			verifySymmetrically(desCBCWithNoPadding, CryptoTestConstants.SIMPLE_RAW_DATA);
			fail("can't get here.");
		} catch (Exception ignored) {
		}
	}

	@Test
	void des_cbc() {
		/* password should be multiple of 8 if we are using DES */
		verifySymmetrically(SymmetricalEncryption.cipher(CipherPattern.of(DES, CBC, PKCS_5).passwordAndIV(CryptoTestConstants.MULTIPLE_OF_8_PASSWORD, CryptoTestConstants.MULTIPLE_OF_8_PASSWORD)), CryptoTestConstants.SIMPLE_RAW_DATA);
	}

	@Test
	void des_ecb() {
		verifySymmetrically(SymmetricalEncryption.cipher(CipherPattern.of(DES, ECB, PKCS_5).password(CryptoTestConstants.MULTIPLE_OF_8_PASSWORD)), CryptoTestConstants.SIMPLE_RAW_DATA);
	}

	public static void verifySymmetrically(SymmetricalEncryption symmetricalEncryption, String rawData) {
		byte[] raw = rawData.getBytes();
		byte[] encrypted = symmetricalEncryption.forward().process(raw);
		byte[] decrypted = symmetricalEncryption.backward().process(encrypted);
		assertArrayEquals(raw, decrypted);
	}
}
