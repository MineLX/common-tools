package io.gitee.minelx.commontools.encrypt;

import org.junit.jupiter.api.Test;

import static io.gitee.minelx.commontools.encrypt.SymmetricalEncryptionTest.verifySymmetrically;

class Base64SymmetricalEncryptionTest {
	@Test
	void verify() {
		verifySymmetrically(SymmetricalEncryption.base64(), "1");
	}
}
