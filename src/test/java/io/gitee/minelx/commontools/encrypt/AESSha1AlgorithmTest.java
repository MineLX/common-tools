package io.gitee.minelx.commontools.encrypt;

import io.gitee.minelx.commontools.encrypt.cipher.algorithm.Algorithm;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class AESSha1AlgorithmTest {
	@Test
	void algorithm() {
		assertEquals("AES", Algorithm.aes_sha1().name());
	}
}
