package io.gitee.minelx.commontools.encrypt;

import io.gitee.minelx.commontools.encrypt.cipher.CipherConstant;
import io.gitee.minelx.commontools.encrypt.cipher.CipherPattern;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CipherPatternTest {

	private CipherPattern pattern;

	@BeforeEach
	void setUp() {
		pattern = CipherPattern.from("DES/CBC/NoPadding");
	}

	@Test
	void of() {
		CipherPattern cipherPattern = CipherPattern.of("DES", "CBC", CipherConstant.Padding.PKCS_5);

	}

	@Test
	void pattern() {
		assertEquals("DES/CBC/NoPadding", pattern.pattern());
	}

	@Test
	void algorithm() {
		assertEquals("DES", pattern.algorithm().name());
	}
}