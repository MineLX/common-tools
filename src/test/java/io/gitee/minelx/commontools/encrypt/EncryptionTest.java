package io.gitee.minelx.commontools.encrypt;

import io.gitee.minelx.commontools.encrypt.cipher.CipherPattern;
import org.junit.jupiter.api.Test;

import static io.gitee.minelx.commontools.encrypt.SymmetricalEncryptionTest.*;
import static io.gitee.minelx.commontools.encrypt.cipher.CipherConstant.Algorithm.DES;
import static io.gitee.minelx.commontools.encrypt.cipher.CipherConstant.Padding.PKCS_5;
import static io.gitee.minelx.commontools.encrypt.cipher.CipherConstant.Strategy.CBC;

class EncryptionTest {

	@Test
	void then() {
		SymmetricalEncryption desCbc = SymmetricalEncryption.cipher(CipherPattern.of(DES, CBC, PKCS_5).passwordAndIV(CryptoTestConstants.MULTIPLE_OF_8_PASSWORD, CryptoTestConstants.MULTIPLE_OF_8_PASSWORD));
		verifySymmetrically(desCbc.then(SymmetricalEncryption.base64()), CryptoTestConstants.SIMPLE_RAW_DATA);
	}
}
