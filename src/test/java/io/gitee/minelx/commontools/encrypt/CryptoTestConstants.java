package io.gitee.minelx.commontools.encrypt;

public class CryptoTestConstants {
	public static final String SIMPLE_RAW_DATA = "1";

	public static final String MULTIPLE_OF_8_RAW_DATA = "12345678";

	public static final String MULTIPLE_OF_8_PASSWORD = "12345678";

	public static final String MULTIPLE_OF_16_PASSWORD = "1234567812345678";
}
