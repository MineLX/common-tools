package io.gitee.minelx.commontools;

import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class FilesTest {
	@Test
	void toString_() {
		assertEquals("content", Files.toString(new ByteArrayInputStream("content".getBytes())));
	}
}
