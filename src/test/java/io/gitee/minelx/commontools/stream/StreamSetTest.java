package io.gitee.minelx.commontools.stream;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class StreamSetTest {

	private static final String FIRST = "1";

	private static final String SECOND = "2";

	private StreamSet<String> streamSet;

	@BeforeEach
	void setUp() {
		streamSet = StreamSet.obtain(Arrays.stream(new String[]{FIRST, SECOND}));
	}

	@Test
	void map() {
		assertEquals("[1, 2]", streamSet.map(Integer::parseInt).all().toString());
	}

	@Test
	void filter() {
		List<String> all = streamSet.filter(which -> which.equals(FIRST)).all();
		assertEquals(1, all.size());
		assertEquals(FIRST, all.get(0));
	}

	@Test
	void all() {
		List<String> all = streamSet.all();
		assertEquals(2, all.size());
		assertEquals(FIRST, all.get(0));
		assertEquals(SECOND, all.get(1));
	}

	@Test
	void first() {
		assertEquals("1", streamSet.first());
	}
}
