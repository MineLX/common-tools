package io.gitee.minelx.commontools.reflection;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class RClassTest {
//	@Test
//	void newInstance__with_args() {
//		RClass<PublicInner> publicInner = RClass.create(PublicInner.class);
//		verify(publicInner.newInstance(1));
//	}

	@Test
	void newInstance() {
		RClass<PublicInner> publicInner = RClass.className("io.gitee.minelx.commontools.reflection.RClassTest$PublicInner");
		verify(publicInner.newInstance());
	}

	private void verify(PublicInner instance) {
		assertEquals(PublicInner.class, instance.getClass());
	}

	@SuppressWarnings("unused")
	public static class PublicInner {
		private int field1;

		public PublicInner() {
		}

		public PublicInner(int field1) {
			this.field1 = field1;
		}
	}
}
