package io.gitee.minelx.commontools;

import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.nio.charset.Charset;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class IOUtilTest {

	public static final String CONTENT = "content";

	@Test
	void bytesOf() throws IOException {
		assertEquals(CONTENT, new String(IOUtil.bytesOf(streamOf(CONTENT))));
	}

	@Test
	void stringOf() throws IOException {
		assertEquals(CONTENT, IOUtil.stringOf(streamOf(CONTENT), Charset.defaultCharset()));
	}

	private ByteArrayInputStream streamOf(String content) {
		return new ByteArrayInputStream(content.getBytes());
	}
}
