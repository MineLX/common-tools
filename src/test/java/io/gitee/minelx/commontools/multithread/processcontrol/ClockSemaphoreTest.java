package io.gitee.minelx.commontools.multithread.processcontrol;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;

class ClockSemaphoreTest {
	public static void main(String[] args) throws InterruptedException {
		Semaphore semaphore = new Semaphore(1, true);
		ExecutorService executorService = Executors.newSingleThreadExecutor();
		executorService.submit(() -> {
			// acquire here
			try {
				semaphore.acquire();
			} catch (InterruptedException e) {
				throw new RuntimeException("error while doing...", e);
			}
			try {
				Thread.sleep(20 * 1000);
			} catch (InterruptedException e) {
				throw new RuntimeException("error while doing...", e);
			}
		});

		System.out.println("release semaphore after 2 seconds...");
		Thread.sleep(2000);

		semaphore.release();

		executorService.shutdown();
	}
}