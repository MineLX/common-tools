package io.gitee.minelx.commontools.multithread;

import org.junit.jupiter.api.Test;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static org.junit.jupiter.api.Assertions.*;

class CallbackTestTest {
	@Test
	void multi_done() throws InterruptedException {
		CallbackTask task = new CallbackTask(2);
		doneWithDelay(task, 500);
		doneWithDelay(task, 1000);
		task.waitForCompletion();
		assertEquals(task.completedCount(), 2);
	}

	@Test
	void simple_test() throws InterruptedException {
		CallbackTask task = new CallbackTask();
		assertFalse(task.isDone());

		doneWithDelay(task, 500);

		task.waitForCompletion();
		assertTrue(task.isDone());
	}

	private void doneWithDelay(CallbackTask task, int delay) {
		ExecutorService executorService = Executors.newSingleThreadExecutor();
		executorService.submit(() -> {
			try {
				Thread.sleep(delay);
			} catch (InterruptedException ignored) {
			}
			System.out.println("call done()");
			task.done();
		});
		executorService.shutdown();
	}
}
