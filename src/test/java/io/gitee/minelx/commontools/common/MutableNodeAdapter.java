package io.gitee.minelx.commontools.common;

import javax.swing.tree.DefaultMutableTreeNode;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

public class MutableNodeAdapter implements Node<DefaultMutableTreeNode> {
	private final DefaultMutableTreeNode target;

	public MutableNodeAdapter(DefaultMutableTreeNode target) {
		this.target = target;
	}

	@Override
	public List<? extends Node<DefaultMutableTreeNode>> children() {
		Enumeration<DefaultMutableTreeNode> enumeration = target.children();
		List<MutableNodeAdapter> result = new ArrayList<>();
		while (enumeration.hasMoreElements()) {
			result.add(new MutableNodeAdapter(enumeration.nextElement()));
		}
		return result;
	}

	@Override
	public DefaultMutableTreeNode get() {
		return target;
	}
}
