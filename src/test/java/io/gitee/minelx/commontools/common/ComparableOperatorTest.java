package io.gitee.minelx.commontools.common;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ComparableOperatorTest {

    private ComparableOperator<Integer> operator;

    @BeforeEach
    void setUp() {
        operator = new ComparableOperator<>(1);
    }

    @Test
    void lt() {
        assertTrue(operator.lt(2));
        assertFalse(operator.lt(1));
    }

    @Test
    void gt() {
        assertTrue(operator.gt(0));
        assertFalse(operator.gt(1));
    }

    @Test
    void le() {
        assertTrue(operator.le(2));
        assertTrue(operator.le(1));
        assertFalse(operator.le(0));
    }

    @Test
    void ge() {
        assertTrue(operator.ge(0));
        assertTrue(operator.ge(1));
        assertFalse(operator.ge(2));
    }

    @Test
    void eq() {
        assertTrue(operator.eq(1));
        assertFalse(operator.eq(0));
    }
}