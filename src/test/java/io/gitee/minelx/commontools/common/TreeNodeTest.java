package io.gitee.minelx.commontools.common;

import org.junit.jupiter.api.Test;

import javax.swing.tree.DefaultMutableTreeNode;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TreeNodeTest {
	@Test
	void simple_test() {
		MutableNodeAdapter rootAdapter = new MutableNodeAdapter(new DefaultMutableTreeNode());
		TreeNode<DefaultMutableTreeNode> rootNode = new TreeNode<>(rootAdapter);
		DefaultMutableTreeNode result = rootNode.convert(DefaultMutableTreeNode::new, DefaultMutableTreeNode::add);
		assertEquals(rootAdapter, result.getUserObject());
	}
}
