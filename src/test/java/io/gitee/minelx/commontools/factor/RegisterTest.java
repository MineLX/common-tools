package io.gitee.minelx.commontools.factor;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

class RegisterTest {

	private Register register;

	@BeforeEach
	void setUp() {
		register = Register.of(Arrays.asList(2, 2));
	}

	@Test
	void size() {
		assertEquals(4, register.size());
	}

	@Test
	void at__should_throw_if_index_out_of_range() {
		try {
			register.at(-1);
			fail("can't get here.");
		} catch (IndexOutOfBoundsException ignored) {
		}
		try {
			register.at(4);
			fail("can't get here.");
		} catch (IndexOutOfBoundsException ignored) {
		}
	}

	@Test
	void at() {
		assertEquals("[0, 0]", register.at(0).toString());
		assertEquals("[0, 1]", register.at(1).toString());
		assertEquals("[1, 0]", register.at(2).toString());
		assertEquals("[1, 1]", register.at(3).toString());
	}
}