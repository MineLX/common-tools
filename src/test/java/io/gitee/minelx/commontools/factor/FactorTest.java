package io.gitee.minelx.commontools.factor;

import io.gitee.minelx.commontools.stream.StreamHelper;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static java.util.stream.Collectors.joining;
import static org.junit.jupiter.api.Assertions.assertEquals;

class FactorTest {
	@Test
	void simple_test() {
		Factor<String> listFactor = Factor.fromList(Arrays.asList("1", "2"));
		assertEquals("1, 2", StreamHelper.orderedStream(listFactor.iterator()).collect(joining(", ")));
	}
}