package io.gitee.minelx.commontools.factor;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static io.gitee.minelx.commontools.stream.StreamHelper.orderedStream;
import static java.util.stream.Collectors.joining;
import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

class RegisterIteratorTest {

    private CloseableIterator<List<String>> registerIterator;

    @BeforeEach
    void setUp() {
        registerIterator = RegisterIterator.create(Arrays.asList(
                Factor.fromList(Arrays.asList("0", "1")),
                Factor.fromList(Arrays.asList("a", "b")),
                Factor.fromList(Arrays.asList("!", "#", "%"))));
    }

    @Test
    void hasNext__empty_factor() {
        List<Factor<String>> factors = Arrays.asList(
                Factor.fromList(Arrays.asList("1", "2")),
                Factor.fromList(Collections.emptyList()));
        try (CloseableIterator<List<String>> registerIterator = RegisterIterator.create(factors)) {
            assertFalse(registerIterator.hasNext());
        }
    }

    @Test
    void hasNext__and__next() {
        assertEquals(
                "[0, a, !], [0, a, #], [0, a, %], [0, b, !], [0, b, #], [0, b, %], [1, a, !], [1, a, #], [1, a, %], [1, b, !], [1, b, #], [1, b, %]",
                orderedStream(registerIterator).map(Object::toString).collect(joining(", ")));
    }

    @AfterEach
    void tearDown() {
        registerIterator.close();
    }
}