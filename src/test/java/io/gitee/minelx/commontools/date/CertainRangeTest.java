package io.gitee.minelx.commontools.date;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class CertainRangeTest {
	@Test
	void init() {
		String type = "MONTH";
		String start = "20210101";
		String end = "20210201";
		CertainRange certainRange = new CertainRange(type, start, end);
		assertCertainRange(certainRange, type, start, end);
	}

	static void assertCertainRange(CertainRange certainRange, String type, String start, String end) {
		assertEquals(type, certainRange.getType());
		assertEquals(start, certainRange.getStart());
		assertEquals(end, certainRange.getEnd());
	}
}