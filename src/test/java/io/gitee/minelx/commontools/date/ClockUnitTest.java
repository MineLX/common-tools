package io.gitee.minelx.commontools.date;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;

import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;

import static io.gitee.minelx.commontools.date.ClockUnit.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

class ClockUnitTest {

    private Calendar instance;

    @BeforeEach
    void setUp() {
        instance = testCalendar();
    }

    @Test
    void at() {
        assertEquals("ClockValues{year=1, month=1, day=1, hour=0, minute=0, second=0, millisecond=0}", YEAR.at(1).values().toString());
        assertEquals("ClockValues{year=1, month=2, day=1, hour=0, minute=0, second=0, millisecond=0}", MONTH.at(1, 2).values().toString());
        assertEquals("ClockValues{year=1, month=2, day=3, hour=0, minute=0, second=0, millisecond=0}", DAY.at(1, 2, 3).values().toString());
        assertEquals("ClockValues{year=1, month=2, day=3, hour=4, minute=0, second=0, millisecond=0}", HOUR.at(1, 2, 3, 4).values().toString());
        assertEquals("ClockValues{year=1, month=2, day=3, hour=4, minute=5, second=0, millisecond=0}", MINUTE.at(1, 2, 3, 4, 5).values().toString());
        assertEquals("ClockValues{year=1, month=2, day=3, hour=4, minute=5, second=6, millisecond=0}", SECOND.at(1, 2, 3, 4, 5, 6).values().toString());
        assertEquals("ClockValues{year=1, month=2, day=3, hour=4, minute=5, second=6, millisecond=7}", MILLISECOND.at(1, 2, 3, 4, 5, 6, 7).values().toString());

        // values count mismatch to the accuracy of ClockUnit
        assertThrows(IllegalArgumentException.class, () -> YEAR.at(1, 2));
        assertThrows(IllegalArgumentException.class, () -> MONTH.at(1));
    }

    @Test
    void isDefaultValue() {
        Calendar calendar = testCalendar(1, 2, 1, 2, 0, 2, 0);
        assertTrue(YEAR.isDefaultValue(calendar));
        assertFalse(MONTH.isDefaultValue(calendar));
        assertTrue(DAY.isDefaultValue(calendar));
        assertFalse(HOUR.isDefaultValue(calendar));
        assertTrue(MINUTE.isDefaultValue(calendar));
        assertFalse(SECOND.isDefaultValue(calendar));
        assertTrue(MILLISECOND.isDefaultValue(calendar));
    }

    @Test
    void set__stash_the_levels_below_and_set_the_levels_remaining() {
        YEAR.set(instance, new int[]{7});
        verifyCalendar(instance, 7, Calendar.JANUARY, 1, 0, 0, 0, 0);
        MONTH.set(instance, new int[]{7, 6});
        verifyCalendar(instance, 7, Calendar.JUNE, 1, 0, 0, 0, 0);
        DAY.set(instance, new int[]{7, 6, 5});
        verifyCalendar(instance, 7, Calendar.JUNE, 5, 0, 0, 0, 0);
        HOUR.set(instance, new int[]{7, 6, 5, 4});
        verifyCalendar(instance, 7, Calendar.JUNE, 5, 4, 0, 0, 0);
        MINUTE.set(instance, new int[]{7, 6, 5, 4, 3});
        verifyCalendar(instance, 7, Calendar.JUNE, 5, 4, 3, 0, 0);
        SECOND.set(instance, new int[]{7, 6, 5, 4, 3, 2});
        verifyCalendar(instance, 7, Calendar.JUNE, 5, 4, 3, 2, 0);
        MILLISECOND.set(instance, new int[]{7, 6, 5, 4, 3, 2, 1});
        verifyCalendar(instance, 7, Calendar.JUNE, 5, 4, 3, 2, 1);
    }

    @Test
    void levelsOnTop() {
        assertEquals(Arrays.asList(YEAR, MONTH, DAY, HOUR, MINUTE, SECOND, MILLISECOND), MILLISECOND.levelsRemaining());
        assertEquals(Arrays.asList(YEAR, MONTH, DAY, HOUR, MINUTE, SECOND), SECOND.levelsRemaining());
        assertEquals(Arrays.asList(YEAR, MONTH, DAY, HOUR, MINUTE), MINUTE.levelsRemaining());
        assertEquals(Arrays.asList(YEAR, MONTH, DAY, HOUR), HOUR.levelsRemaining());
        assertEquals(Arrays.asList(YEAR, MONTH, DAY), DAY.levelsRemaining());
        assertEquals(Arrays.asList(YEAR, MONTH), MONTH.levelsRemaining());
        assertEquals(Collections.singletonList(YEAR), YEAR.levelsRemaining());
    }

    @Test
    void initValue() {
        assertEquals(0, MILLISECOND.defaultValue());
        assertEquals(0, SECOND.defaultValue());
        assertEquals(0, MINUTE.defaultValue());
        assertEquals(0, HOUR.defaultValue());
        assertEquals(1, DAY.defaultValue());
        assertEquals(1, MONTH.defaultValue());
        assertEquals(1, YEAR.defaultValue());
    }

    @Test
    void levelsBelow() {
        assertEquals(Collections.emptyList(), MILLISECOND.levelsBelow());
        assertEquals(Collections.singletonList(MILLISECOND), SECOND.levelsBelow());
        assertEquals(Arrays.asList(
                SECOND,
                MILLISECOND), MINUTE.levelsBelow());
        assertEquals(Arrays.asList(
                MINUTE,
                SECOND,
                MILLISECOND), HOUR.levelsBelow());
        assertEquals(Arrays.asList(
                HOUR,
                MINUTE,
                SECOND,
                MILLISECOND), DAY.levelsBelow());
        assertEquals(Arrays.asList(
                DAY,
                HOUR,
                MINUTE,
                SECOND,
                MILLISECOND), MONTH.levelsBelow());
        assertEquals(Arrays.asList(
                MONTH,
                DAY,
                HOUR,
                MINUTE,
                SECOND,
                MILLISECOND), YEAR.levelsBelow());
    }

    @Test
    void set() {
        MILLISECOND.set(instance, 1);
        assertEquals(1, instance.get(Calendar.MILLISECOND));
        SECOND.set(instance, 1);
        assertEquals(1, instance.get(Calendar.SECOND));
        MINUTE.set(instance, 1);
        assertEquals(1, instance.get(Calendar.MINUTE));
        HOUR.set(instance, 1);
        assertEquals(1, instance.get(Calendar.HOUR_OF_DAY));
        DAY.set(instance, 1);
        assertEquals(1, instance.get(Calendar.DAY_OF_MONTH));
        MONTH.set(instance, 1);
        assertEquals(Calendar.JANUARY, instance.get(Calendar.MONTH));
        YEAR.set(instance, 1);
        assertEquals(1, instance.get(Calendar.YEAR));
    }

    @Test
    void add() {
        MILLISECOND.add(instance, 1);
        assertEquals(8, instance.get(Calendar.MILLISECOND));
        SECOND.add(instance, 1);
        assertEquals(7, instance.get(Calendar.SECOND));
        MINUTE.add(instance, 1);
        assertEquals(6, instance.get(Calendar.MINUTE));
        HOUR.add(instance, 1);
        assertEquals(5, instance.get(Calendar.HOUR_OF_DAY));
        DAY.add(instance, 1);
        assertEquals(4, instance.get(Calendar.DAY_OF_MONTH));
        MONTH.add(instance, 1);
        assertEquals(Calendar.MARCH, instance.get(Calendar.MONTH));
        YEAR.add(instance, 1);
        assertEquals(2, instance.get(Calendar.YEAR));
    }

    private static Calendar testCalendar() {
        return testCalendar(1, Calendar.FEBRUARY, 3, 4, 5, 6, 7);
    }

    static Calendar testCalendar(int year, int month, int day, int hour, int minute, int second, int mills) {
        Calendar result = Calendar.getInstance();
        // all 7 units set
        result.set(year, month, day, hour, minute, second);
        result.set(Calendar.MILLISECOND, mills);
        return result;
    }

    static void verifyCalendar(Calendar calendar, int year, int month, int day, int hour, int minute, int second, int millisecond) {
        assertEquals(year, calendar.get(Calendar.YEAR));
        assertEquals(month, calendar.get(Calendar.MONTH));
        assertEquals(day, calendar.get(Calendar.DAY_OF_MONTH));
        assertEquals(hour, calendar.get(Calendar.HOUR_OF_DAY));
        assertEquals(minute, calendar.get(Calendar.MINUTE));
        assertEquals(second, calendar.get(Calendar.SECOND));
        assertEquals(millisecond, calendar.get(Calendar.MILLISECOND));
    }
}