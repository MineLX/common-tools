package io.gitee.minelx.commontools.date;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;
import static org.junit.jupiter.api.Assertions.assertEquals;

class MonthTest {

	private Month month;

	@BeforeEach
	void setUp() {
		month = Month.of(2022, 1);
	}

	@Test
	void equals_() {
		assertEquals(month, Month.of(2022, 1));
	}

	@Test
	void days() {
		List<Day> daysOfThisMonth = month.days().collect(toList());
		assertEquals(31, daysOfThisMonth.size());
		assertEquals("20220101, 20220102, 20220103, 20220104, 20220105, 20220106, 20220107, 20220108, 20220109, 20220110, 20220111, 20220112, 20220113, 20220114, 20220115, 20220116, 20220117, 20220118, 20220119, 20220120, 20220121, 20220122, 20220123, 20220124, 20220125, 20220126, 20220127, 20220128, 20220129, 20220130, 20220131",
				daysOfThisMonth.stream()
						.map(day -> day.format("yyyyMMdd"))
						.collect(joining(", ")));
	}

	@Test
	void format() {
		assertEquals("202201", month.format("yyyyMM"));
	}

	@Test
	void months() {
		assertEquals("202201, 202202, 202203, 202204, 202205, 202206, 202207, 202208, 202209, 202210, 202211, 202212, 202301, 202302, 202303",
				month.months()
						.map(month -> month.format("yyyyMM"))
						.limit(15) // contains 15 month
						.collect(joining(", ")));
	}

	@Test
	void offset() {
		assertEquals("202202", month.offset(1)
				.format("yyyyMM"));
		// end of this year
		assertEquals("202301", Month.of(2022, 12)
				.offset(1)
				.format("yyyyMM"));
	}

	@Test
	void day() {
		assertEquals("20220101", month.day(1).format("yyyyMMdd"));
	}

	@Test
	void init() {
		assertEquals("202201", month.format("yyyyMM"));
	}
}