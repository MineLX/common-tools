package io.gitee.minelx.commontools.date;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Calendar;

import static io.gitee.minelx.commontools.date.Elapses._locate;
import static io.gitee.minelx.commontools.date.Elapses._offset;
import static io.gitee.minelx.commontools.date.ClockUnit.DAY;
import static io.gitee.minelx.commontools.date.ClockUnitTest.verifyCalendar;

public class ElapsesTest {

    private Calendar calendar;

    @BeforeEach
    void setUp() {
        calendar = ClockUnitTest.testCalendar(1, Calendar.FEBRUARY, 3, 4, 5, 6, 7);
    }

    @Test
    void at() {
        Elapses._locate(DAY, 1).accept(calendar);
        verifyCalendar(calendar, 1, Calendar.FEBRUARY, 1, 4, 5, 6, 7);
    }

    @Test
    void offset() {
        _offset(1, DAY).accept(calendar);
        verifyCalendar(calendar, 1, Calendar.FEBRUARY, 4, 4, 5, 6, 7);
    }
}
