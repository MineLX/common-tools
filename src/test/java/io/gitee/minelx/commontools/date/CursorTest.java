package io.gitee.minelx.commontools.date;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

class CursorTest {

	private Cursor<String> cursor;

	@BeforeEach
	void setUp() {
		cursor = new Cursor<>(Arrays.asList("1", "2"));
	}

	@Test
	void revert() {
		cursor.pop();
		cursor.revert();
		assertEquals("1", cursor.lookup());
		try {
			cursor.revert();
			fail("can't reach here.");
		} catch (IllegalStateException ignored) {
		}
	}

	@Test
	void popThem() {
		assertEquals("[1]", cursor.popThem(which -> which.equals("1")).toString());
	}

	@Test
	void hasNext() {
		assertTrue(cursor.hasNext());
		cursor.pop();
		cursor.pop();
		assertFalse(cursor.hasNext());
	}

	@Test
	void pop() {
		assertEquals("1", cursor.pop());
		assertEquals("2", cursor.pop());
		try {
			cursor.pop();
			fail("can't reach here.");
		} catch (IllegalStateException ignored) {
		}
	}

	@Test
	void lookup() {
		assertEquals("1", cursor.lookup());
	}
}