package io.gitee.minelx.commontools.date.example;

import io.gitee.minelx.commontools.date.Clocks;

public class WhatsInClocks {
    public static void main(String[] args) {
        String now = Clocks.now().format("yyyy/MM/dd hh:mm:ss");
        System.out.println("now = " + now);

        String millis = Clocks.millis(1640966400000L).format("yyyy/MM/dd hh:mm:ss");
        System.out.println("millis = " + millis);

        String unixTime = Clocks.unixTime(1640966400).format("yyyy/MM/dd hh:mm:ss");
        System.out.println("unixTime = " + unixTime);
    }
}
