package io.gitee.minelx.commontools.date;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Calendar;

import static io.gitee.minelx.commontools.date.Elapses._locate;
import static io.gitee.minelx.commontools.date.Elapses._offset;
import static java.util.Calendar.YEAR;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class ClockTest {

	private Clock clock;

	@BeforeEach
	void setUp() {
		clock = ClockUnit.DAY.at(2022, 5, 1);
	}

	@Test
	void to() {
		assertEquals("Duration{start=Clock{values=ClockValues{year=2022, month=5, day=1, hour=0, minute=0, second=0, millisecond=0}}, end=Clock{values=ClockValues{year=2022, month=6, day=1, hour=0, minute=0, second=0, millisecond=0}}}", clock.to(ClockUnit.MONTH.at(2022, 6)).toString());
	}

	@Test
	void ceil() {
		assertEquals("20230101", clock.ceil(ClockUnit.YEAR).format("yyyyMMdd")); // ceil to next year
		assertEquals("20220501", clock.ceil(ClockUnit.MONTH).format("yyyyMMdd")); // remains what it used to be
	}

	@Test
	void equals_() {
		assertEquals(clock, ClockUnit.DAY.at(2022, 5, 1));
	}

	@Test
	void locate() {
		assertEquals("ClockValues{year=2022, month=5, day=2, hour=0, minute=0, second=0, millisecond=0}",
				clock.elapse(Elapses._locate(ClockUnit.DAY, 2)).values().toString());
		assertEquals("ClockValues{year=2022, month=2, day=1, hour=0, minute=0, second=0, millisecond=0}",
				clock.elapse(Elapses._locate(ClockUnit.MONTH, 2)).values().toString());
	}

	@Test
	void toString_() {
		assertEquals("ClockValues{year=2022, month=5, day=1, hour=0, minute=0, second=0, millisecond=0}",
				clock.values().toString());
	}

	@Test
	void offset() {
		assertEquals("ClockValues{year=2022, month=5, day=2, hour=0, minute=0, second=0, millisecond=0}",
				clock.elapse(_offset(1, ClockUnit.DAY)).values().toString());
		assertEquals("ClockValues{year=2022, month=6, day=1, hour=0, minute=0, second=0, millisecond=0}",
				clock.elapse(_offset(1, ClockUnit.MONTH)).values().toString());
	}

	@Test
	void self() {
		assertEquals(2022, clock.self().get(YEAR));
		assertEquals(5, clock.self().get(Calendar.MONTH) + 1);
		assertEquals(1, clock.self().get(Calendar.DAY_OF_MONTH));
	}

	@Test
	void init() {
		assertEquals(2022, clock.values().year());
		assertEquals(5, clock.values().month());
		assertEquals(1, clock.values().day());
	}
}