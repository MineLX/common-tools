package io.gitee.minelx.commontools.date;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static io.gitee.minelx.commontools.date.ClockUnit.MONTH;
import static java.util.stream.Collectors.joining;
import static org.junit.jupiter.api.Assertions.assertEquals;

class DurationTest {

    private Duration smallDuration;

    private Duration accurateDuration;

    @BeforeEach
    void setUp() {
        smallDuration = Duration.between(
                ClockUnit.DAY.at(2022, 1, 31),
                ClockUnit.DAY.at(2022, 2, 2));
        accurateDuration = Duration.between(
                ClockUnit.HOUR.at(2022, 1, 31, 18),
                ClockUnit.HOUR.at(2022, 2, 2, 18));
    }

    @Test
    void clocks() {
        assertEquals("20220131", smallDuration.clocks(MONTH, 1)
                .map(clock -> clock.format("yyyyMMdd"))
                .collect(joining(", ")));
    }

    @Test
    void toString_() {
        assertEquals("Duration{start=Clock{values=ClockValues{year=2022, month=1, day=31, hour=0, minute=0, second=0, millisecond=0}}, end=Clock{values=ClockValues{year=2022, month=2, day=2, hour=0, minute=0, second=0, millisecond=0}}}", smallDuration.toString());
    }

    @Test
    void months() {
        assertEquals("202202", smallDuration.months()
                .map(day -> day.format("yyyyMM"))
                .collect(joining(", ")));
    }

    @Test
    void days__should_ignore_the_start_point_if_it_have_more_accuracies() {
        assertEquals("20220201, 20220202", accurateDuration.days()
                .map(day -> day.format("yyyyMMdd"))
                .collect(joining(", ")));
    }

    @Test
    void days() {
        assertEquals("20220131, 20220201", smallDuration.days()
                .map(day -> day.format("yyyyMMdd"))
                .collect(joining(", ")));
    }
}