package io.gitee.minelx.commontools.date;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ClocksTest {
    @Test
    void unixTime() {
        assertEquals("Clock{values=ClockValues{year=2022, month=1, day=1, hour=0, minute=0, second=0, millisecond=0}}", Clocks.unixTime(1640966400).toString());
    }

    @Test
    void millis() {
        assertEquals("Clock{values=ClockValues{year=2022, month=1, day=1, hour=0, minute=0, second=0, millisecond=0}}", Clocks.millis(1640966400000L).toString());
    }

    @Test
    void text() {
        assertEquals("Clock{values=ClockValues{year=2022, month=1, day=1, hour=0, minute=0, second=0, millisecond=0}}", Clocks.text("yyyyMMdd", "20220101").toString());
    }
}