package io.gitee.minelx.commontools.date;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ClockValuesTest {

    private ClockValues clockValues;

    @BeforeEach
    void setUp() {
        clockValues = new ClockValues(1, 2, 3, 4, 5, 6, 7);
    }

    @Test
    void op() {
        assertTrue(clockValues.op().gt(new ClockValues(1, 2, 3, 4, 5, 6, 0)));
        assertTrue(clockValues.op().eq(clockValues));
        assertTrue(clockValues.op().lt(new ClockValues(2, 2, 3, 4, 5, 6, 7)));
    }

    @Test
    void compareTo() {
        assertEquals(7, clockValues.compareTo(new ClockValues(1, 2, 3, 4, 5, 6, 0)));
        assertEquals(0, clockValues.compareTo(new ClockValues(1, 2, 3, 4, 5, 6, 7)));
        assertEquals(-1, clockValues.compareTo(new ClockValues(2, 2, 3, 4, 5, 6, 7)));
    }

    @Test
    void range() {
        assertArrayEquals(new int[]{1, 2}, clockValues.range(2));
    }

    @Test
    void init() {
        assertEquals(1, clockValues.year());
        assertEquals(2, clockValues.month());
        assertEquals(3, clockValues.day());
        assertEquals(4, clockValues.hour());
        assertEquals(5, clockValues.minute());
        assertEquals(6, clockValues.second());
        assertEquals(7, clockValues.millisecond());
    }
}