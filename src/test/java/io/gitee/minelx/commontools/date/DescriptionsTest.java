package io.gitee.minelx.commontools.date;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static java.util.Collections.singletonList;
import static org.junit.jupiter.api.Assertions.assertEquals;

class DescriptionsTest {
	@Test
	void split__mixed_with_two_types_of_CertainRange() {
		assertDescriptions(
				Arrays.asList(
						new Description("20210101"),
						new Description("20210201"),
						new Description("20210202")),
				Arrays.asList(
						new CertainRange("MONTH", "20210101", "20210201"),
						new CertainRange("DAY", "20210201", "20210202")));
	}

	@Test
	void split__we_will_get_more_months_if_there_are_more_months() {
		assertDescriptions(
				Arrays.asList(
						new Description("20210101"),
						new Description("20210201"),
						new Description("20210301")),
				singletonList(
						new CertainRange("MONTH", "20210101", "20210301")));
	}

	@Test
	void split__we_will_get_a_single_month() {
		assertDescriptions(
				Arrays.asList(
						new Description("20210101"),
						new Description("20210201")),
				singletonList(
						new CertainRange("MONTH", "20210101", "20210201")));
	}

	@Test
	void split__there_is_a_month_cross_line_between_us() {
		assertDescriptions(
				Arrays.asList(
						new Description("20210102"),
						new Description("20210201"),
						new Description("20210202")),
				Arrays.asList(
						new CertainRange("DAY", "20210102", "20210201"),
						new CertainRange("DAY", "20210201", "20210202")));
	}

	@Test
	void split__we_will_get_a_single_day_range_if_we_at_the_same_month() {
		assertDescriptions(
				Arrays.asList(
						new Description("20210101"),
						new Description("20210102")),
				singletonList(
						new CertainRange("DAY", "20210101", "20210102")));
	}

	private void assertDescriptions(List<Description> descriptions, List<CertainRange> result) {
		assertEquals(result, new Descriptions(descriptions).split());
	}
}