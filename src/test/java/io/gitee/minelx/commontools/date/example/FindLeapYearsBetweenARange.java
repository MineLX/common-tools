package io.gitee.minelx.commontools.date.example;

import static io.gitee.minelx.commontools.date.Elapses._locate;
import static io.gitee.minelx.commontools.date.Elapses._offset;
import static io.gitee.minelx.commontools.date.ClockUnit.*;

class FindLeapYearsBetweenARange {
    public static void main(String[] args) {
        long yyyyMM = YEAR.at(2000).to(YEAR.at(3000))
                .clocks(YEAR, 1)
                .map(each -> each.elapse(_locate(MONTH, 3).andThen(_offset(-1, DAY))))
                .filter(which -> which.values().day() > 28)
                .map(each -> each.format("yyyy"))
                .count();
        System.out.println("yyyyMM = " + yyyyMM);
    }
}
