package io.gitee.minelx.commontools.date;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static java.util.Collections.singletonList;
import static org.junit.jupiter.api.Assertions.assertEquals;

class DayRangeTest {
	@Test
	void split__DAY_MONTH_DAY() {
		assertSplit("20210102", "20210302",
				Arrays.asList(
						new CertainRange("DAY", "20210102", "20210201"),
						new CertainRange("MONTH", "20210201", "20210301"),
						new CertainRange("DAY", "20210301", "20210302")
				));
	}

	@Test
	void split__mixed_with_two_types_of_CertainRange() {
		assertSplit("20210101", "20210202",
				Arrays.asList(
						new CertainRange("MONTH", "20210101", "20210201"),
						new CertainRange("DAY", "20210201", "20210202")
				));
	}

	@Test
	void split__we_will_get_more_months_if_there_are_more_months() {
		assertSplit("20210101", "20210301",
				singletonList(
						new CertainRange("MONTH", "20210101", "20210301")
				));
	}

	@Test
	void split__we_will_get_a_single_month() {
		assertSplit("20210101", "20210201",
				singletonList(
						new CertainRange("MONTH", "20210101", "20210201")
				));
	}

	@Test
	void split__there_is_a_month_cross_line_between_us() {
		assertSplit("20210102", "20210202",
				Arrays.asList(
						new CertainRange("DAY", "20210102", "20210201"),
						new CertainRange("DAY", "20210201", "20210202")
				));
	}

	@Test
	void split__we_will_get_a_single_day_range_if_we_at_the_same_month() {
		assertSplit("20210101", "20210102", singletonList(new CertainRange("DAY", "20210101", "20210102")));
	}

	private void assertSplit(String start, String end, List<CertainRange> expected) {
		assertEquals(expected, new DateRange(start, end).split());
	}
}
