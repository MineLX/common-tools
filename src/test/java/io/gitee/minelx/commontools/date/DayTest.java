package io.gitee.minelx.commontools.date;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;
import static org.junit.jupiter.api.Assertions.*;

class DayTest {

	private Day day;

	@BeforeEach
	void setUp() {
		day = Day.from("20220501");
	}

	@Test
	void month() {
		assertEquals("202205", day.month().format("yyyyMM"));
	}

	@Test
	void format() {
		assertEquals("20220501", day.format("yyyyMMdd"));
	}

	@Test
	void days() {
		assertEquals("20220501, 20220502", day.days().limit(2).collect(toList()).stream()
				.map(day1 -> day1.format("yyyyMMdd"))
				.collect(joining(", ")));
	}

	@Test
	void days__between() {
		assertEquals("20220501, 20220502", day.daysBetween(Day.of(2022, 5, 3)).collect(toList()).stream()
				.map(day1 -> day1.format("yyyyMMdd"))
				.collect(joining(", ")));
	}

	@Test
	void offset() {
		assertEquals("20220502", day.offset(1).format("yyyyMMdd"));
		assertEquals("20220501", day
				.offset(-1)
				.offset(1).format("yyyyMMdd"));
	}
}