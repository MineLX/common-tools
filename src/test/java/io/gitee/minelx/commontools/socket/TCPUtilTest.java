package io.gitee.minelx.commontools.socket;

import io.gitee.minelx.commontools.multithread.CallbackTask;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.net.Socket;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TCPUtilTest {
	public static final int PORT = 8880;

	@Test
	void once() throws IOException, InterruptedException {
		CallbackTask task = new CallbackTask();
		TCPPortService service = new TCPPortService(PORT, received -> {
			assertEquals("message", new String(received));
			task.done();
		});
		service.start();
		try (Socket target = new Socket("localhost", PORT)) {
			TCPUtil.once(target, "message");
		}
		task.waitForCompletion();
		service.stop();
	}
}
