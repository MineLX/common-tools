package io.gitee.minelx.commontools.socket;

import io.gitee.minelx.commontools.IOUtil;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;

public class TCPUtil {
	// FIXME: 2021/1/15  NEEDED REFACTORING !!!  to class
	public static void once(Socket socket, String message) throws IOException {
		OutputStream outputStream = socket.getOutputStream();
		ByteArrayInputStream inputStream = new ByteArrayInputStream(message.getBytes());
		IOUtil.exchange(inputStream, outputStream);
	}
}
