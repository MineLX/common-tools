package io.gitee.minelx.commontools.multithread.processcontrol;

public interface SemaphoreHelper {
	void release();

	void acquire();

	static SemaphoreHelper limit(int maxPermits, int permitsReleasingInterval) {
		return new ClockSemaphore(maxPermits, permitsReleasingInterval);
	}

	static SemaphoreHelper unlimited() {
		return new SemaphoreHelper() {
			@Override
			public void release() {
			}

			@Override
			public void acquire() {
			}
		};
	}
}
