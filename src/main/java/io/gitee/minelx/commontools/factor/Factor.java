package io.gitee.minelx.commontools.factor;

import java.util.List;

public interface Factor<E> {
	CloseableIterator<E> iterator();

	static <E> Factor<E> fromList(List<E> elements) {
		return () -> CloseableIterator.fromIterator(elements.iterator());
	}
}
