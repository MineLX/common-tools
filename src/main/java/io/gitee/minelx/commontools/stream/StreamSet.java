package io.gitee.minelx.commontools.stream;

import java.util.List;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;

public class StreamSet<E> {
	private Stream<E> stream;

	private StreamSet(Stream<E> stream) {
		this.stream = stream;
	}

	public StreamSet<E> filter(Predicate<E> predicate) {
		stream = stream.filter(predicate);
		return this;
	}

	public <R> StreamSet<R> map(Function<E, R> mapper) {
		return obtain(stream.map(mapper));
	}

	public E first() {
		return stream.findFirst().orElseThrow(RuntimeException::new);
	}

	public List<E> all() {
		return stream.collect(toList());
	}

	public static <E> StreamSet<E> obtain(Stream<E> stream) {
		return new StreamSet<>(stream);
	}
}
