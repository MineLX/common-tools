package io.gitee.minelx.commontools.date;

import java.util.Calendar;
import java.util.function.Consumer;

public final class Elapses {
    public static Consumer<Calendar> _offset(int amount, ClockUnit unit) {
        return calendar -> unit.add(calendar, amount);
    }

    public static Consumer<Calendar> _offset(ClockUnit unit, int amount) {
        return _offset(amount, unit);
    }

    public static Consumer<Calendar> _locate(ClockUnit unit, int amount) {
        return calendar -> unit.set(calendar, amount);
    }

    public static Consumer<Calendar> _locate(int amount, ClockUnit unit) {
        return _locate(unit, amount);
    }
}
