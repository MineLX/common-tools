package io.gitee.minelx.commontools.date;

import java.util.Objects;
import java.util.stream.Stream;

import static io.gitee.minelx.commontools.date.Elapses._locate;
import static io.gitee.minelx.commontools.date.Elapses._offset;
import static io.gitee.minelx.commontools.date.ClockUnit.DAY;
import static io.gitee.minelx.commontools.date.ClockUnit.MONTH;

public class Month {

	private final Clock clock;

	Month(Clock clock) {
		this.clock = clock;
	}

	public Stream<Day> days() {
		Day firstDayOfThisMonth = day(1);
		Day firstDayOfNextMonth = offset(1).day(1);
		return firstDayOfThisMonth.daysBetween(firstDayOfNextMonth);
	}

	public Stream<Month> months() {
		return clock.offsets(MONTH, 1).map(Month::new);
	}

	public Day day(int at) {
		return clock.elapse(_locate(DAY, at)).day();
	}

	public String format(String pattern) {
		return clock.format(pattern);
	}

	public Month offset(int amount) {
		return new Month(clock.elapse(_offset(amount, MONTH)));
	}

	public static Month of(int year, int month) {
		return new Month(MONTH.at(year, month));
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Month month = (Month) o;
		return clock.equals(month.clock);
	}

	@Override
	public int hashCode() {
		return Objects.hash(clock);
	}
}
