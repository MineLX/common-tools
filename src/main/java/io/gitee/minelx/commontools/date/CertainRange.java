package io.gitee.minelx.commontools.date;

import java.util.Objects;

public class CertainRange {
	private final String type;

	private final String start;

	private final String end;

	public CertainRange(String type, String start, String end) {
		this.type = type;
		this.start = start;
		this.end = end;
	}

	public String getType() {
		return type;
	}

	public String getStart() {
		return start;
	}

	public String getEnd() {
		return end;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		CertainRange that = (CertainRange) o;
		return type.equals(that.type) && start.equals(that.start) && end.equals(that.end);
	}

	@Override
	public int hashCode() {
		return Objects.hash(type, start, end);
	}

	@Override
	public String toString() {
		return "CertainRange{" +
				"type='" + type + '\'' +
				", start='" + start + '\'' +
				", end='" + end + '\'' +
				'}';
	}
}
