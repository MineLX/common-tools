package io.gitee.minelx.commontools.date;

public class Description {
	private final String atDescription;

	public Description(String atDescription) {
		this.atDescription = atDescription;
	}

	public boolean isFirstDayOfAMonth() {
		return atDescription.endsWith("01");
	}

	public String getAtDescription() {
		return atDescription;
	}
}
