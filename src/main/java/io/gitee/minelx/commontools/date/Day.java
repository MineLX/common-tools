package io.gitee.minelx.commontools.date;

import java.util.stream.Stream;

import static io.gitee.minelx.commontools.date.Elapses._offset;

public class Day {

	public static final long DAY_AS_MILLIS = 86400000L;

	private final Clock clock;

	Day(Clock clock) {
		this.clock = clock;
	}

	public String format(String pattern) {
		return clock.format(pattern);
	}

	public Stream<Day> days() {
		return clock.offsets(ClockUnit.DAY, 1).map(Day::new);
	}

	public Stream<Day> daysBetween(Day destination) {
		return days().limit(dayCountsBetween(destination));
	}

	public Day offset(long dayCounts) {
		return clock.elapse(_offset((int) dayCounts, ClockUnit.DAY)).day();
	}

	public long dayCountsBetween(Day destination) {
		return (destination.millis() - this.millis()) / DAY_AS_MILLIS;
	}

	public long millis() {
		return clock.self().getTimeInMillis();
	}

	public Month month() {
		return clock.cast(ClockUnit.MONTH).month();
	}

	public static Day of(int year, int month, int day) {
		return ClockUnit.DAY.at(year, month, day).day();
	}

	public static Day from(String text) {
		return Clocks.text("yyyyMMdd", text).day();
	}
}