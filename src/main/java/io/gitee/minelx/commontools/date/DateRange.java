package io.gitee.minelx.commontools.date;

import java.util.ArrayList;
import java.util.List;

import static java.util.stream.Collectors.toList;

public class DateRange {

	private final String start;

	private final String end;

	public DateRange(String start, String end) {
		this.start = start;
		this.end = end;
	}

	public List<CertainRange> split() {
		List<Description> swappedDescriptions = new ArrayList<>();
		swappedDescriptions.add(new Description(start));
		descriptionsBetween(Day.from(start), Day.from(end).offset(1)).stream()
				.filter(Description::isFirstDayOfAMonth)
				.forEach(swappedDescriptions::add);
		// add end
		swappedDescriptions.add(new Description(end));
		return new Descriptions(swappedDescriptions).split();
	}

	private List<Description> descriptionsBetween(Day start, Day end) {
		List<Description> descriptions = start.daysBetween(end).collect(toList()).stream()
				.map(day -> day.format("yyyyMMdd"))
				.map(Description::new)
				.collect(toList());
		return descriptions.subList(1, descriptions.size() - 1);
	}
}
