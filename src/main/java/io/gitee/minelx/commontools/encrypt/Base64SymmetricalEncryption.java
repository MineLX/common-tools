package io.gitee.minelx.commontools.encrypt;

import java.util.Base64;

class Base64SymmetricalEncryption implements SymmetricalEncryption {
	static final Base64SymmetricalEncryption INSTANCE = new Base64SymmetricalEncryption();

	Base64SymmetricalEncryption() {
	}

	@Override
	public Encryption forward() {
		return rawBytes -> Base64.getEncoder().encode(rawBytes);
	}

	@Override
	public Encryption backward() {
		return rawBytes -> Base64.getDecoder().decode(rawBytes);
	}
}
