package io.gitee.minelx.commontools.encrypt.cipher;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import java.security.InvalidKeyException;

class Password implements CipherFactory {

	private final CipherPattern cipherPattern;

	private final SecretKey secretKey;

	Password(CipherPattern cipherPattern, SecretKey secretKey) {
		this.cipherPattern = cipherPattern;
		this.secretKey = secretKey;
	}

	@Override
	public Cipher createCipher(int cryptoMode) {
		try {
			Cipher result = cipherPattern.obtainCipher();
			result.init(cryptoMode, secretKey);
			return result;
		} catch (InvalidKeyException e) {
			throw new RuntimeException("failed to create cipher. ", e);
		}
	}
}
