package io.gitee.minelx.commontools.encrypt.cipher;

public final class CipherConstant {

	public static final class Strategy {
		public static final String CBC = "CBC";
		public static final String ECB = "ECB";
	}

	public static final class Algorithm {
		public static final String DES = "DES";

		public static final String AES = "AES";
	}

	public static final class Padding {
		public static final String PKCS_5 = "PKCS5Padding";

		public static final String NOPE = "NoPadding";
	}
}
