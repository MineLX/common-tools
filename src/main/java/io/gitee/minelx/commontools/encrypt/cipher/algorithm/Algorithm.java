package io.gitee.minelx.commontools.encrypt.cipher.algorithm;

import javax.crypto.SecretKey;

public interface Algorithm {
	SecretKey secretKey(String password);

	String name();

	static Algorithm des() {
		return new DESAlgorithm();
	}

	static Algorithm aes_sha1() {
		return new AESSha1Algorithm();
	}

	static Algorithm from(String text) {
		if (text.equals("DES")) {
			return des();
		} else if (text.equals("AES")) {
			return aes_sha1();
		}
		throw new IllegalArgumentException("unexpected algorithm: " + text + ".");
	}
}
