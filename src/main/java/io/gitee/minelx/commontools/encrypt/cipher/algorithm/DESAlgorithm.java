package io.gitee.minelx.commontools.encrypt.cipher.algorithm;

import io.gitee.minelx.commontools.encrypt.cipher.CipherConstant;

import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

class DESAlgorithm implements Algorithm {

	DESAlgorithm() {
	}

	@Override
	public SecretKey secretKey(String password) {
		try {
			return SecretKeyFactory.getInstance(CipherConstant.Algorithm.DES)
					.generateSecret(new DESKeySpec(password.getBytes()));
		} catch (InvalidKeySpecException | NoSuchAlgorithmException | InvalidKeyException e) {
			throw new RuntimeException("failed to create SecretKey.", e);
		}
	}

	@Override
	public String name() {
		return CipherConstant.Algorithm.DES;
	}
}
