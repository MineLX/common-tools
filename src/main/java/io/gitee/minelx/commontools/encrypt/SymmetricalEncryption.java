package io.gitee.minelx.commontools.encrypt;

import io.gitee.minelx.commontools.encrypt.cipher.CipherFactory;

public interface SymmetricalEncryption {

	Encryption forward();

	Encryption backward();

	static SymmetricalEncryption base64() {
		return Base64SymmetricalEncryption.INSTANCE;
	}

	static SymmetricalEncryption cipher(CipherFactory cipherFactory) {
		return new CipherSymmetricalEncryption(cipherFactory);
	}

	default SymmetricalEncryption then(SymmetricalEncryption another) {
		return new SymmetricalEncryption() {
			@Override
			public Encryption forward() {
				return SymmetricalEncryption.this.forward().then(another.forward());
			}

			@Override
			public Encryption backward() {
				return another.backward().then(SymmetricalEncryption.this.backward());
			}
		};
	}
}
