package io.gitee.minelx.commontools.encrypt.cipher;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;

class PasswordWithIV implements CipherFactory {

	private final CipherPattern cipherPattern;

	private final SecretKey secretKey;

	private final IvParameterSpec ivPassword;

	PasswordWithIV(CipherPattern cipherPattern, SecretKey secretKey, IvParameterSpec ivPassword) {
		this.cipherPattern = cipherPattern;
		this.secretKey = secretKey;
		this.ivPassword = ivPassword;
	}

	@Override
	public Cipher createCipher(int cryptoMode) {
		try {
			Cipher result = cipherPattern.obtainCipher();
			result.init(cryptoMode, secretKey, ivPassword);
			return result;
		} catch (InvalidKeyException | InvalidAlgorithmParameterException e) {
			throw new RuntimeException("failed to create cipher. ", e);
		}
	}
}
