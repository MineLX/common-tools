package io.gitee.minelx.commontools.encrypt;

public interface Encryption {
	byte[] process(byte[] source);

	default Encryption then(Encryption another) {
		return source -> {
			byte[] afterSelf = process(source);
			return another.process(afterSelf);
		};
	}
}
