package io.gitee.minelx.commontools.encrypt.cipher.algorithm;

import io.gitee.minelx.commontools.encrypt.cipher.CipherConstant;

import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

class AESSha1Algorithm implements Algorithm {

	AESSha1Algorithm() {
	}

	@Override
	public SecretKey secretKey(String password) {
		try {
			KeyGenerator keyGenerator = KeyGenerator.getInstance(CipherConstant.Algorithm.AES);
			SecureRandom secureRandom = SecureRandom.getInstance("SHA1PRNG");
			secureRandom.setSeed(password.getBytes());
			keyGenerator.init(256, secureRandom);
			return keyGenerator.generateKey();
		} catch (NoSuchAlgorithmException e) {
			throw new RuntimeException("failed to create SecretKey.", e);
		}
	}

	@Override
	public String name() {
		return CipherConstant.Algorithm.AES;
	}
}
