package io.gitee.minelx.commontools.encrypt.cipher;

import javax.crypto.Cipher;

public interface CipherFactory {
	Cipher createCipher(int cryptoMode);
}
