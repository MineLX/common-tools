package io.gitee.minelx.commontools.common;

import java.util.Collections;
import java.util.List;
import java.util.function.Function;

// FIXME 2021/7/8  wait for me!!!     test
public interface Node<E> {
	List<? extends Node<E>> children();

	E get();

	static <E> Node<E> create(E element, Function<Node<E>, List<? extends Node<E>>> childrenGetter) {
		return new Node<E>() {
			@Override
			public List<? extends Node<E>> children() {
				return childrenGetter.apply(this);
			}

			@Override
			public E get() {
				return element;
			}
		};
	}

	static <E> Node<E> leaf(E element) {
		return new Node<E>() {
			@Override
			public List<? extends Node<E>> children() {
				return Collections.emptyList();
			}

			@Override
			public E get() {
				return element;
			}
		};
	}
}
