package io.gitee.minelx.commontools.common;

public class ComparableOperator<S extends Comparable<S>> {

    private final S self;

    public ComparableOperator(S self) {
        this.self = self;
    }

    public boolean eq(S element) {
        return self.compareTo(element) == 0;
    }

    public boolean gt(S element) {
        return self.compareTo(element) > 0;
    }

    public boolean lt(S element) {
        return self.compareTo(element) < 0;
    }

    public boolean ge(S element) {
        return self.compareTo(element) >= 0;
    }

    public boolean le(S element) {
        return self.compareTo(element) <= 0;
    }
}
